package UnisysV4.com.ProjectTeste.Interface;

import org.springframework.data.repository.CrudRepository;

import UnisysV4.com.ProjectTeste.POJO.Task;

public interface TaskInterface extends CrudRepository<Task, Long> {

}

package UnisysV4.com.ProjectTeste.Interface;

import org.springframework.data.repository.CrudRepository;

import UnisysV4.com.ProjectTeste.POJO.Job;

public interface JobInterface extends CrudRepository<Job, Long> {

}

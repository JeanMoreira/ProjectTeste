package UnisysV4.com.ProjectTeste.Interface;

import java.util.List;



public interface Persistencia<T> {

	public List<T> buscar();
	public T carregar(Integer id);
	public void salvar(T entidade);
	public void excluir(T entidade);
	
}
package UnisysV4.com.ProjectTeste.Infra;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import UnisysV4.com.ProjectTeste.Interface.Persistencia;

@Repository
public class DAO<T> implements Persistencia<T> {

	
	//@PersistenceContext
	//private Class<T> entityClass;
	@Autowired
    private EntityManager em;

   
	
	public List<T> buscar() {
		// TODO Auto-generated method stub
		return null;
	}

	public T carregar(Integer id) {

		return null;
	}

	public void salvar(T entidade) {
		em.getTransaction().begin();
        em.persist(entidade);
        em.getTransaction().commit();
        em.close();
		
	}

	public void excluir(T entidade) {
		// TODO Auto-generated method stub
		
	}

}

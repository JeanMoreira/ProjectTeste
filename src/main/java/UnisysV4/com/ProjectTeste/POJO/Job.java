package UnisysV4.com.ProjectTeste.POJO;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;


@Entity
public class Job {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;// required (integer)
	
	@NotNull
	private String name;// required (string)
	
	@NotNull
	private boolean active;// required (boolean)
	
	@OneToMany(mappedBy = "job", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Task> tasks;
	
	 @ManyToOne(cascade = CascadeType.ALL)
     @JoinColumn(name = "idJobParent", referencedColumnName = "id")
     private Job parent;
	
	
	public Job getParent() {
		return parent;
	}
	public void setParent(Job parent) {
		this.parent = parent;
	}
	public List<Task> getTasks() {
		return tasks;
	}
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	//tasks: (array of Task)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
}
